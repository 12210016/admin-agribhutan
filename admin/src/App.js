import React from "react";
import { BrowserRouter as Router, Route, Routes} from "react-router-dom";

import LoginPage from "./pages/login";
import DashboardPage from "./pages/dashboard";
import VegetablePage from "./pages/vegetables";
import MaterialPage from "./pages/materials";
import UpcomingPage from "./pages/upcoming";
import ReportPage from "./pages/report";
import UserPage from "./pages/user";
import FeedbackPage from "./pages/feedback";
import ProfilePage from "./pages/profile";

const App = () => {
  
  return (
    <Router>
      <Routes>
        <Route path="/" element={<LoginPage/>} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/dashboard" element={<DashboardPage />} />
        <Route path="/vegetables" element={<VegetablePage />} />
        <Route path="/materials" element={<MaterialPage />} />
        <Route path="/upcoming" element={<UpcomingPage />} />
        <Route path="/user" element={<UserPage />} />
        <Route path="/report" element={<ReportPage />} />
        <Route path="/feedback" element={<FeedbackPage />} />
        <Route path="/profile" element={<ProfilePage />} />
      </Routes>
    </Router>
  );
};

export default App;
