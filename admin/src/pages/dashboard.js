import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import "./dashboard.css";
import Sidebar from "../components/sidebar";

const DashboardPage = () => {
  const userCount = 10;
  const vegetablesCount = 20;
  const materialsCount = 30;

  return (
    <div className="dashboard-page">
      <div className="header">
        <FontAwesomeIcon icon={faBars} className="menu-icon" />
        <h1 className="title">Dashboard</h1>
      </div>
      <div className="dashboard">
        <div className="box">
          <div className="count-box">
            <h2>User Count</h2>
            <span className="count">{userCount}</span>
          </div>
          <div className="count-box">
            <h2>Vegetables Count</h2>
            <span className="count">{vegetablesCount}</span>
          </div>
        </div>

        <div className="box">
          <div className="count-box">
            <h2>Materials Count</h2>
            <span className="count">{materialsCount}</span>
          </div>
          <div className="count-box">
            <h2> Upcoming Count</h2>
            <span className="count">{materialsCount}</span>
          </div>
        </div>
      </div>
      <Sidebar />
    </div>
  );
};

export default DashboardPage;
