import React, { useState, useRef } from "react";
import { useNavigate } from "react-router-dom";
import "./login.css";
import Logo from "../img/logo.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faLock } from "@fortawesome/free-solid-svg-icons";

const LoginPage = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const navigateRef = useRef(navigate);

  const handleLogin = (e) => {
    e.preventDefault();
    navigateRef.current("/dashboard");
  };

  return (
    <div className="login-container">
      <form className="login-form" onSubmit={handleLogin}>
        <img className="logo" src={Logo} alt="Logo" />
        <h1 className="login-heading">Login</h1>
        <div className="form-group">
          <label htmlFor="username" className="form-label">
            <FontAwesomeIcon icon={faEnvelope} className="input-icon" />
          </label>
          <input
            type="text"
            id="username"
            className="form-input"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            placeholder="Enter email"
          />
        </div>
        <div className="form-group">
          <label htmlFor="password" className="form-label">
            <FontAwesomeIcon icon={faLock} className="input-icon" />
          </label>
          <input
            type="password"
            id="password"
            className="form-input"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Enter password"
          />
        </div>
        <button type="submit" className="login-button">
          Login
        </button>
      </form>
    </div>
  );
};

export default LoginPage;
