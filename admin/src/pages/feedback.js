import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import "./vegetables.css";
import Sidebar from "../components/sidebar";

const FeedbackPage = () => {
  return (
    <div className="dashboard-page">
      <div className="header">
        <FontAwesomeIcon icon={faBars} className="menu-icon" />
        <h1 className="title">Feedback</h1>
      </div>
      <div className="dashboard">
      <table className="vegetable-table">
          <thead>
            <tr>
              <th>Sl.no</th>
              <th>Name</th>
              <th>Email</th>
              <th>Description</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
      <Sidebar /> 
    </div>
  );
};

export default FeedbackPage;
