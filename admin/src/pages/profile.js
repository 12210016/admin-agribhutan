import React, { useState, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { useDropzone } from "react-dropzone";
import "./profile.css";
import Sidebar from "../components/sidebar";
import Default from "../img/default.png";

const ProfilePage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [image, setImage] = useState(Default);
  const fileInputRef = useRef(null);

  const handleUpdate = () => {
    
  };

  const handleImageClick = () => {
    if (fileInputRef.current) {
      fileInputRef.current.click(); 
    }
  };

  const onDrop = (acceptedFiles) => {
    const selectedImage = acceptedFiles[0];
    const imageURL = URL.createObjectURL(selectedImage);
    setImage(imageURL);
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: "image/*",
    multiple: false,
  });

  return (
    <div className="dashboard-page">
      <div className="header">
        <FontAwesomeIcon icon={faBars} className="menu-icon" />
        <h1 className="title">Profile</h1>
      </div>
      <div className="profile-container">
        <form className="profile-form" onSubmit={handleUpdate}>
          <div className="image-container" {...getRootProps()}>
            <input {...getInputProps()} />
            {isDragActive ? (
              <p>Drop the image here...</p>
            ) : (
              <label htmlFor="image-picker" className="image-label">
                <img
                  className="default"
                  src={image}
                  alt="Default"
                  onClick={handleImageClick}
                />
              </label>
            )}
          </div>
          <div className="form-group">
            <label htmlFor="Email" className="form-label">
              Email:
            </label>
            <input
              className="form-input"
              type="email"
              id="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="form-group">
            <label htmlFor="Password" className="form-label">
              Password:
            </label>
            <input
              className="form-input"
              type="password"
              id="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <button type="submit" className="button">
            Save
          </button>
        </form>
      </div>
      <Sidebar />
    </div>
  );
};

export default ProfilePage;
