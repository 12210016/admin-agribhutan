import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import "./dashboard.css";
import Sidebar from "../components/sidebar";

const ReportPage = () => {
  return (
    <div className="dashboard-page">
      <div className="header">
        <FontAwesomeIcon icon={faBars} className="menu-icon" />
        <h1 className="title">Report</h1>
      </div>
      <div className="dashboard">
      <table className="vegetable-table">
          <thead>
            <tr>
              <th>Sl.no</th>
              <th>SEmail</th>
              <th>SName</th>
              <th>Categories</th>
              <th>Description</th>
              <th>REmail</th>
              <th>RName</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
      <Sidebar /> 
    </div>
  );
};

export default ReportPage;
