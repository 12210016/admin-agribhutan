import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import "./vegetables.css";
import Sidebar from "../components/sidebar";

const UpcomingPage = () => {
  return (
    <div className="dashboard-page">
      <div className="header">
        <FontAwesomeIcon icon={faBars} className="menu-icon" />
        <h1 className="title">Upcoming Products</h1>
      </div>
      <div className="dashboard">
        <table className="vegetable-table">
          <thead>
            <tr>
              <th>Sl.no</th>
              <th>Email</th>
              <th>Product Name</th>
              <th>Price(Nu)</th>
              <th>Location</th>
              <th>Estimated Quantity(Kg)</th>
              <th>Contact</th>
              <th>Available Date</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
      <Sidebar />
    </div>
  );
};

export default UpcomingPage;
