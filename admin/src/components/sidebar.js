import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChartBar,
  faComments,
  faUser,
  faUserCircle,
  faCalendar,
  faSignOutAlt,
  faFileAlt,
  faBoxes,
  faCarrot,
} from "@fortawesome/free-solid-svg-icons";
import { NavLink } from "react-router-dom";
import "./sidebar.css";

const Sidebar = () => {

  const handleLogout = () => {
    // Perform logout logic here, if any
    window.location.href = "/login";
  };

  return (
    <div>
      <div className="sidebar">
        <ul className="sidebar-nav">
        <h1 className="heading">Agri-Bhutan</h1>
          <li className="sidebar-item">
          <NavLink exact to="/dashboard" className="sidebar-link" activeClassName="active">
              <div>
                <FontAwesomeIcon icon={faChartBar} className="sidebar-icon" />
                <span className="sidebar-text">Dashboard</span>
              </div>
            </NavLink>
          </li>

          <li className="sidebar-item">
          <NavLink to="/vegetables" className="sidebar-link" activeClassName="active">
              <div>
                <FontAwesomeIcon icon={faCarrot} className="sidebar-icon" />
                <span className="sidebar-text">Vegetables</span>
              </div>
            </NavLink>
          </li>

          <li className="sidebar-item">
            <NavLink to="/materials" className="sidebar-link" activeClassName="active">
              <div>
                <FontAwesomeIcon icon={faBoxes} className="sidebar-icon" />
                <span className="sidebar-text">Materials</span>
              </div>
            </NavLink>
          </li>

          <li className="sidebar-item">
            <NavLink to="/upcoming" className="sidebar-link" activeClassName="active">
              <div>
                <FontAwesomeIcon icon={faCalendar} className="sidebar-icon" />
                <span className="sidebar-text">Upcoming</span>
              </div>
            </NavLink>
          </li>

          <li className="sidebar-item">
            <NavLink to="/user" className="sidebar-link" activeClassName="active">
              <div>
                <FontAwesomeIcon icon={faUser} className="sidebar-icon" />
                <span className="sidebar-text">Users</span>
              </div>
            </NavLink>
          </li>

          <li className="sidebar-item">
            <NavLink to="/report" className="sidebar-link" activeClassName="active">
              <div>
                <FontAwesomeIcon icon={faFileAlt} className="sidebar-icon" />
                <span className="sidebar-text">Report</span>
              </div>
            </NavLink>
          </li>

          <li className="sidebar-item">
            <NavLink to="/feedback" className="sidebar-link" activeClassName="active">
              <div>
                <FontAwesomeIcon icon={faComments} className="sidebar-icon" />
                <span className="sidebar-text">Feedback</span>
              </div>
            </NavLink>
          </li>

          <li className="sidebar-item">
            <NavLink to="/profile" className="sidebar-link" activeClassName="active">
              <div>
                <FontAwesomeIcon icon={faUserCircle} className="sidebar-icon" />
                <span className="sidebar-text">Profile</span>
              </div>
            </NavLink>
          </li>

          <li className="sidebar-item">
            <NavLink to="/login" className="sidebar-link" onClick={handleLogout}   activeClassName="active">
              <div>
                <FontAwesomeIcon icon={faSignOutAlt} className="sidebar-icon" />
                <span className="sidebar-text">Logout</span>
              </div>
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
